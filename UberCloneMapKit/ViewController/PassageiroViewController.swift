//
//  PassageiroViewController.swift
//  UberCloneMapKit
//
//  Created by Wagner Rodrigues on 11/12/2017.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import UIKit
import FirebaseAuth
import MapKit
import FirebaseDatabase

class PassageiroViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var areaEndereco: UIView!
    @IBOutlet weak var marcadorLocalPassageiro: UIView!
    @IBOutlet weak var marcadorLocalDestino: UIView!
    @IBOutlet weak var enderecoDestinoCampo: UITextField!
    
    
    @IBOutlet weak var btnChamar: UIButton!
    @IBOutlet weak var mapa: MKMapView!
    var gerenciadorLocalizacao = CLLocationManager()
    var localUsuario = CLLocationCoordinate2D()
    var localMotorista = CLLocationCoordinate2D()
    var uberChamado = false
    var uberACaminho = false
    
    @IBAction func deslogarUsuario(_ sender: Any) {
        let autenticacao = Auth.auth()
        do {
            try autenticacao.signOut()
            dismiss(animated: true, completion: nil)
        } catch  {
            print("Não foi possével deslogar!")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        gerenciadorLocalizacao.delegate = self
        gerenciadorLocalizacao.desiredAccuracy = kCLLocationAccuracyBest
        gerenciadorLocalizacao.requestWhenInUseAuthorization()
        gerenciadorLocalizacao.startUpdatingLocation()
        gerenciadorLocalizacao.allowsBackgroundLocationUpdates = true 
        
        //CONFIGURA ARREDONDAMENTO DOS MARCADORES
        self.marcadorLocalPassageiro.layer.cornerRadius = 7.5
        self.marcadorLocalPassageiro.clipsToBounds = true

        self.marcadorLocalDestino.layer.cornerRadius = 7.5
        self.marcadorLocalDestino.clipsToBounds = true
        
        self.areaEndereco.layer.cornerRadius = 10
        self.areaEndereco.clipsToBounds = true
        
        // VERIFICA SE JA TEM UMA REQUISICAO DE UBER
        let dataBase = Database.database().reference()
        let autenticacao = Auth.auth()
        
        if let emailUsuario = autenticacao.currentUser?.email{
            let requisicoes = dataBase.child("requisicoes")
            let consultaRequisicoes = requisicoes
                .queryOrdered(byChild: "email")
                .queryEqual(toValue: emailUsuario)
            //ADD OUVINTE PARA QUANDO O USER CHAMAR O UBER
            consultaRequisicoes.observe(.childAdded, with: { (snapshot) in
                if snapshot.value != nil{
                    self.cancelarUber()
                }
            })
            
            //ADD OUVINTE PARA QUANDO O MOTORISTA ACEITAR A CORRIDA
            consultaRequisicoes.observe(.childChanged, with: { (snapshot) in
                if let dados = snapshot.value as? [String: Any]{
                    
                    if let status = dados["status"] as? String{
                        if status == StatusCorrida.PegarPassageiro.rawValue{
                            if let latMotorista = dados["motoristaLatitude"]{
                                if let lonMotorisra = dados["motoristaLongitude"]{
                                    self.localMotorista = CLLocationCoordinate2D(latitude: latMotorista as! CLLocationDegrees, longitude: lonMotorisra as! CLLocationDegrees)
                                    
                                    self.exibirMotoristaPassageiro()
                                }
                            }
                        }else if(status == StatusCorrida.EmViagem.rawValue){
                            self.alternarBtnEmViagem()
                        }else if(status == StatusCorrida.ViagemFinalizada.rawValue){
                            if let preco = dados["precoViagem"] as? Double{
                                self.alternarBtnViagemFinalizada(preco: preco)
                            }
                        }
                    }
                    

                    
                }
            })
        }
    }
    
    func exibirMotoristaPassageiro(){
        
        self.uberACaminho = true
        
        //CALCULA A DISTANCIA ENTRE MOTORISTA E PASSAGEIRO
        
        let motoristaLocation = CLLocation(latitude: self.localMotorista.latitude, longitude: self.localMotorista.longitude)
        let passageiroLocation = CLLocation(latitude: self.localUsuario.latitude, longitude: self.localUsuario.longitude)
        var mensagem = ""
        let distancia  = motoristaLocation.distance(from: passageiroLocation)
        let distanciaKM = distancia / 1000
        let distanciaFinal = round(distanciaKM)
        mensagem = "Motorista \(distanciaFinal) KM distante"
        
        if distanciaKM < 1{
            let distanciaM = round(distancia)
            mensagem = "Motorista \(distanciaM) metros distante"
        }
        self.btnChamar.backgroundColor = UIColor(displayP3Red: 0.067, green: 0.576, blue: 0.604, alpha: 1)
        self.btnChamar.setTitle(mensagem, for: .normal)
        
        //EXIBE PASSAGEIRO E MOTORISTA NO MAPA
        
        mapa.removeAnnotations(mapa.annotations)
        
        let latDiferenca = abs(self.localUsuario.latitude - self.localMotorista.latitude) * 300000
        let lonDiferenca = abs(self.localUsuario.longitude  - self.localMotorista.longitude) * 300000
        
        let regiao = MKCoordinateRegionMakeWithDistance(self.localUsuario, latDiferenca, lonDiferenca)
        mapa.setRegion(regiao, animated: true)
        
        
        //ANOTACAO MOTORISTA
        let anotacaoMotorista = MKPointAnnotation()
        anotacaoMotorista.coordinate = self.localMotorista
        anotacaoMotorista.title = "Motorista"
        mapa.addAnnotation(anotacaoMotorista)
        
        //ANOTACAO PASSAGEIRO
        let anotacaoPassageiro = MKPointAnnotation()
        anotacaoPassageiro.coordinate = self.localUsuario
        anotacaoPassageiro.title = "Passageiro"
        mapa.addAnnotation(anotacaoPassageiro)
    }
    
    @IBAction func chamarUber(_ sender: Any) {
        let dataBase = Database.database().reference()
        let autenticacao = Auth.auth()
        let requisicao = dataBase.child("requisicoes")
        
        if let emailUsuario = autenticacao.currentUser?.email{
            
            if self.uberChamado{//UBER CHAMADO
                self.chamarUber()
                //REMOVER REQUISICAO
                let requisicao = dataBase.child("requisicoes")
                
                requisicao.queryOrdered(byChild: "email")
                    .queryEqual(toValue: emailUsuario)
                    .observeSingleEvent(of: DataEventType.childAdded, with: { (snapshot) in
                        
                        snapshot.ref.removeValue()
                    })
            }else{//UBER NAO FOI CHAMADO
                self.salvarRequisicao()
            }//fim else
        }
    }
    
    func salvarRequisicao() {
        let dataBase = Database.database().reference()
        let autenticacao = Auth.auth()
        
        let requisicao = dataBase.child("requisicoes")
        
        if let idUsuario = autenticacao.currentUser?.uid{
            if let emailUsuario = autenticacao.currentUser?.email{
                if let enderecoDestino = self.enderecoDestinoCampo.text {
                    if enderecoDestino != "" {
                        CLGeocoder().geocodeAddressString(enderecoDestino, completionHandler: { (local, erro) in
                            if erro == nil {
                                if let dadosLocal = local?.first{
                                    var rua = ""
                                    if dadosLocal.thoroughfare != nil {
                                        rua = dadosLocal.thoroughfare!
                                    }
                                    var numero = ""
                                    if dadosLocal.subThoroughfare != nil {
                                        numero = dadosLocal.subThoroughfare!
                                    }
                                    var bairro = ""
                                    if dadosLocal.subLocality != nil {
                                        bairro = dadosLocal.subLocality!
                                    }
                                    var cidade = ""
                                    if dadosLocal.locality != nil {
                                        cidade = dadosLocal.locality!
                                    }
                                    var cep = ""
                                    if dadosLocal.postalCode != nil {
                                        cep = dadosLocal.postalCode!
                                    }
                                    
                                    let enderecoCompleto = "\(rua), \(numero), \(bairro) - \(cidade) - \(cep)"
                                    
                                    if let latDestino = dadosLocal.location?.coordinate.latitude{
                                        if let lonDestino = dadosLocal.location?.coordinate.longitude{
                                            let alerta = UIAlertController(title: "Confirme seu endereço!", message: enderecoCompleto, preferredStyle: .alert)
                                            let acaoCancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
                                            let acaoConfirmar = UIAlertAction(title: "Confirmar", style: .default, handler: { (alertAction) in
                                                //RECUPERAR O NOME DO USUARIO
                                                
                                                let dataBase = Database.database().reference()
                                                let usuarios = dataBase.child("usuarios").child(idUsuario)
                                                
                                                usuarios.observeSingleEvent(of: .value, with: { (snapshot) in
                                                    let dados = snapshot.value as? NSDictionary
                                                    let nomeUsuario = dados!["nome"] as? String
                                                    
                                                    self.cancelarUber()
                                                    //SALVAR DADOS DA REQUISICAO
                                                    let dadosUsuario = [
                                                        "destinoLatitude" : latDestino,
                                                        "destinoLongitude" : lonDestino,
                                                        "email" : emailUsuario,
                                                        "nome" : nomeUsuario,
                                                        "latitude" : self.localUsuario.latitude,
                                                        "longitude" : self.localUsuario.longitude
                                                        ] as [String : Any]
                                                    
                                                    requisicao.childByAutoId().setValue(dadosUsuario)
                                                    self.cancelarUber()
                                                })
                                                
                                                
                                            })
                                            
                                            alerta.addAction(acaoCancelar)
                                            alerta.addAction(acaoConfirmar)
                                            
                                            self.present( alerta, animated: true, completion: nil )
                                            
                                        }//fim lonDestino
                                    }//fim latDestino
                                }
                                
                            }
                        })
                        
                    }else{
                        print("Endereco não digitado!")
                    }
                }
                
            }//fim if emailUsuario
        }//fim if idUsuario
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //RECUPERA AS COORDENADAS DO LOCAL ATUAL
        if let coordenadas = manager.location?.coordinate{
            
            //RECUPERA LOCAL ATUAL DO USUARIO
            self.localUsuario = coordenadas
            
            if self.uberACaminho{
                self.exibirMotoristaPassageiro()
            }else{
                let regiao = MKCoordinateRegionMakeWithDistance(coordenadas, 200, 200)
                mapa.setRegion(regiao, animated: true)
                
                //REMOVE ANOTACOES ANTES DE CRIAR
                mapa.removeAnnotations(mapa.annotations)
                
                //CRIA UMA ANOTACAO PARA O LOCAL ATUAL
                let anotacaoUsuario = MKPointAnnotation()
                anotacaoUsuario.coordinate = coordenadas
                anotacaoUsuario.title = "Seu local"
                mapa.addAnnotation(anotacaoUsuario)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alternarBtnViagemFinalizada(preco: Double) {
        
        self.btnChamar.isEnabled = false
        self.btnChamar.backgroundColor = UIColor(displayP3Red: 0.502, green: 0.502, blue: 0.502, alpha: 1)
        
        //FORMATA NUMERO
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.maximumFractionDigits = 2
        nf.locale = Locale(identifier: "pt_BR")
        
        let precoFinal = nf.string(from: NSNumber(value: preco))
        self.btnChamar.setTitle("Viagem finalizada - R$" + precoFinal!, for: .normal)
    }
    
    
    func alternarBtnEmViagem () {
        self.btnChamar.setTitle("Em viagem", for: .normal)
        self.btnChamar.isEnabled = false
        self.btnChamar.backgroundColor = UIColor(displayP3Red: 0.502, green: 0.502, blue: 0.502, alpha: 1)
        
    }
    
    func cancelarUber() {
        self.btnChamar.setTitle("Cancelar Uber", for: .normal)
        self.uberChamado = true
        self.btnChamar.backgroundColor = UIColor(displayP3Red: 0.831, green: 0.237, blue: 0.146, alpha: 1)

    }
    
    func chamarUber()  {
        self.btnChamar.setTitle("Chamar Uber", for: .normal)
        self.uberChamado = false
        self.btnChamar.backgroundColor = UIColor(displayP3Red: 0.076, green: 0.576, blue: 0.604, alpha: 1)    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
