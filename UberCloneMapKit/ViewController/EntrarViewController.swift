//
//  EntrarViewController.swift
//  UberCloneMapKit
//
//  Created by Wagner Rodrigues on 11/12/2017.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import UIKit
import FirebaseAuth

class EntrarViewController: UIViewController {
    
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var senha: UITextField!
    
    
    @IBAction func entrar(_ sender: Any) {
        let retorno = self.validarCampos()
        if retorno == "" {
            //FAZ AUTENTICACAO DO USUARIO (LOGIN)
            let autenticacao = Auth.auth()
            if let emailR = self.email.text{
                    if let senhaR = self.senha.text{
                        autenticacao.signIn(withEmail: emailR, password: senhaR, completion: { (usuario, erro) in
                            if erro == nil {
                                /*
                                 VALIDA SE O USUARIO ESTA LOGADO
                                 CASO O USUARIO ESTEJA LOGADO, SERÁ REDICERIONADOS AUTOMATICAMENTE DE ACORDO COM O EVENTO CRIADO NA VIEW CONTROLLER
                                 */
                                if usuario == nil{
                                    print("Erro ao logar Usuário")
                                }
                            }else{
                                print("Erro ao autenticar usuário, tente novamente!")
                            }
                        })
                    }
            }
        }else{
            print("O campo\(retorno) não foi preenchido!")
        }
    }
    
    func validarCampos() -> String{
        
        if (self.email.text?.isEmpty)!{
            return "E-mail"
        }else if (self.senha.text?.isEmpty)!{
            return "Senha"
        }
        return ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
