//
//  CadastroViewController.swift
//  UberCloneMapKit
//
//  Created by Wagner Rodrigues on 11/12/2017.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class CadastroViewController: UIViewController {
    
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var nomeCompleto: UITextField!
    @IBOutlet weak var senha: UITextField!
    @IBOutlet weak var tipoUsuario: UISwitch!
    
    
    @IBAction func cadastrarUsuario(_ sender: Any) {
        let retorno = self.validarCampos()
        if retorno == ""{
            //CADASTRAR USUARIO NO FIREBASE
            let autenticacao = Auth.auth()
            
            if let emailR = self.email.text{
                if let nomeR = self.nomeCompleto.text{
                    if let senhaR = self.senha.text{
                        autenticacao.createUser(withEmail: emailR, password: senhaR, completion: { (usuario, erro) in
                            
                            if erro == nil{
                                
                                //CONFIGURA DATABASE
                                let dataBase = Database.database().reference()
                                let usuarios = dataBase.child("usuarios")
                                
                                
                                //VALIDA SE O USUARIO ESTA LOGADO
                                if usuario != nil{
                                    //VERIFICA O TIPO DO USUARIO
                                    var tipo = ""
                                    if self.tipoUsuario.isOn{
                                        tipo = "passageiro"
                                    }else{
                                        tipo = "motorista"
                                    }
                                    
                                    //SALVAR NO DB OS DADOS DO USUSARIO
                                    let dadosUsuario = [
                                        "email" : usuario?.email,
                                        "nome" : nomeR,
                                        "tipo" : tipo
                                    ]
                                    //SALVAR USUARIOS
                                    usuarios.child((usuario?.uid)!).setValue(dadosUsuario)
                                    /*
                                     VALIDA SE O USUARIO ESTA LOGADO
                                     CASO O USUARIO ESTEJA LOGADO, SERÁ REDICERIONADOS AUTOMATICAMENTE DE ACORDO COM O EVENTO CRIADO NA VIEW CONTROLLER
                                     */
                                    //self.performSegue(withIdentifier: "segueLoginCadastro", sender: nil)
                                }else{
                                    print("Erro ao autenticar usuario")
                                }
                                
                            }else{
                                print("Erro ao criar conta do usuário, tente mais tarde!")
                            }
                        })
                        
                    }
                }
            }
        }else{
            print("O campo \(retorno) não foi preenchido!")
        }
    }
    
    func validarCampos() -> String{
        
        if (self.email.text?.isEmpty)!{
            return "E-mail"
        }else if (self.nomeCompleto.text?.isEmpty)!{
            return "Nome Completo"
        }else if (self.senha.text?.isEmpty)!{
            return "Senha"
        }
        return ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
