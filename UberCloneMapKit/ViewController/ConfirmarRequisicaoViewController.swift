//
//  ConfirmarRequisicaoViewController.swift
//  UberCloneMapKit
//
//  Created by Wagner Rodrigues on 14/12/2017.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import UIKit
import MapKit
import FirebaseDatabase
import FirebaseAuth



class ConfirmarRequisicaoViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var mapa: MKMapView!
    @IBOutlet weak var btnAceitarCorrida: UIButton!
    var gerenciadorLocalizacao = CLLocationManager()
    
    var nomePassageiro = ""
    var emailPassageiro = ""
    var localPassageiro = CLLocationCoordinate2D()
    var localMotorista = CLLocationCoordinate2D()
    var localDestino = CLLocationCoordinate2D()
    var status: StatusCorrida = .EmRequisicao
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let coordenadas = manager.location?.coordinate{
            
            self.localMotorista = coordenadas
            self.atualizarLocalMotorista()
        }
        
    }
    
    func atualizarLocalMotorista(){
        
        //ATUALIZAR LOCALIZACAO DO MOTORISTA NO FIREBASE
        let dataBase = Database.database().reference()
        
        if self.emailPassageiro != ""{
            
            let requisicoes = dataBase.child("requisicoes")
            let consultaRequisicao = requisicoes.queryOrdered(byChild: "email")
                                                .queryEqual(toValue: emailPassageiro)
            
            consultaRequisicao.observeSingleEvent(of: DataEventType.childAdded, with: { (snapshot) in
                if let dados = snapshot.value as? [String: Any] {
                    if let statusR = dados["status"] as? String {
                        
                        //STATUS PEGARPAGGAGEIRO
                        if statusR == StatusCorrida.PegarPassageiro.rawValue{
                            
                            //VERIFICA SE O MOTORISTA ESTA PROXIMO, PARA INICIAR A CORRIDA
                            let motoristaLocation = CLLocation(latitude: self.localMotorista.latitude, longitude: self.localMotorista.longitude)
                            let passageiroLocation = CLLocation(latitude: self.localPassageiro.latitude, longitude: self.localPassageiro.longitude)
                            
                            //CALCULA DISTANCIA ENTRE MOTORISTA E PASSAGEIRO
                            let distancia = motoristaLocation.distance(from: passageiroLocation)
                            let distanciaKM = distancia / 1000
                            
                            
                            if distanciaKM <= 0.5 {
                                //ATUALIZAR STATUS
                                self.atualizarStatusRequisicao(status: StatusCorrida.IniciarViagem.rawValue)
                            }
                            
                            
                            
                        }else if(statusR == StatusCorrida.IniciarViagem.rawValue){
//                            self.alternarBtnIniciarViagem()
                            
                            //EXIBIR MOTORISTA PASSAGEIRO
                            self.exibeMotoristaPassageiro(lPartida: self.localMotorista, lDestino: self.localPassageiro, tPartida: "Motorista", tDestino: "Passageiro")


                        }else if(statusR == StatusCorrida.EmViagem.rawValue){
                            if let latDestino = dados["destinoLatitude"] as? Double{
                                if let lonDestino = dados["destinoLongitude"] as? Double{
                                    self.localDestino = CLLocationCoordinate2D(latitude: latDestino, longitude: lonDestino )
                                    //EXIBIR MOTORISTA PASSAGEIRO
                                    self.exibeMotoristaPassageiro(lPartida: self.localMotorista, lDestino: self.localDestino, tPartida: "Motorista", tDestino: "Destino")
                                    
                                }
                            }
                        }
                    }
                    let dadosMotorista = [
                        "motoristaLatitude" : self.localMotorista.latitude,
                        "motoristaLongitude" : self.localMotorista.longitude
                        ] as [String : Any]
                    
                    //SALVAR DADOS NO FIREBASE
                    snapshot.ref.updateChildValues(dadosMotorista)
                    
                }
            })
        }
        
    }
    
    func atualizarStatusRequisicao(status: String) {
        if status != "" && self.emailPassageiro != "" {
            
            let dataBase = Database.database().reference()
            let requisicoes = dataBase.child("requisicoes")
            let consultaRequisicao = requisicoes.queryOrdered(byChild: "email")
                                                .queryEqual(toValue: self.emailPassageiro)
            
            consultaRequisicao.observeSingleEvent(of: DataEventType.childAdded, with: { (snapshot) in
                if let dados = snapshot.value as? [String: Any]{
                    let dadosAtualizar = [
                        "status" : status
                    ]
                    snapshot.ref.updateChildValues(dadosAtualizar)
                }
            })
        }
    }
    
    func exibeMotoristaPassageiro(lPartida: CLLocationCoordinate2D, lDestino: CLLocationCoordinate2D, tPartida: String, tDestino: String)  {
        //EXIBE PASSAGEIRO E MOTORISTA NO MAPA
        
        mapa.removeAnnotations(mapa.annotations)
        
        let latDiferenca = abs(lPartida.latitude - lDestino.latitude) * 300000
        let lonDiferenca = abs(lPartida.longitude  - lDestino.longitude) * 300000
        
        let regiao = MKCoordinateRegionMakeWithDistance(lPartida, latDiferenca, lonDiferenca)
        mapa.setRegion(regiao, animated: true)
        
        
        //ANOTACAO PARTIDA
        let anotacaoPartida = MKPointAnnotation()
        anotacaoPartida.coordinate = lPartida
        anotacaoPartida.title = tPartida
        mapa.addAnnotation(anotacaoPartida)
        
        //ANOTACAO DESTINO
        let anotacaoDestino = MKPointAnnotation()
        anotacaoDestino.coordinate = lDestino
        anotacaoDestino.title = tDestino
        mapa.addAnnotation(anotacaoDestino)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        gerenciadorLocalizacao.delegate = self
        gerenciadorLocalizacao.desiredAccuracy = kCLLocationAccuracyBest
        gerenciadorLocalizacao.requestWhenInUseAuthorization()
        gerenciadorLocalizacao.startUpdatingLocation()
        gerenciadorLocalizacao.allowsBackgroundLocationUpdates = true 
        
        
        //CONFIGURAR AREA INICIAL DO MAPA
        let regiao = MKCoordinateRegionMakeWithDistance(localPassageiro, 200, 200)
        
        mapa.setRegion(regiao, animated: true)
        
        
        
        //ADD ANOTACAO PARA PASSAGEIRO
        let anotacaoPassageiro = MKPointAnnotation()
        anotacaoPassageiro.coordinate = self.localPassageiro
        anotacaoPassageiro.title = self.nomePassageiro
        mapa.addAnnotation(anotacaoPassageiro)
        
        //RECUPERA STATUS E AJUSTA INTERFACE
        let datataBase = Database.database().reference()
        
        let requisicoes = datataBase.child("requisicoes")
        let consultaRequisicoes = requisicoes.queryOrdered(byChild: "email")
            .queryEqual(toValue: self.emailPassageiro)
        
        consultaRequisicoes.observe(DataEventType.childChanged, with: { (snapshot) in
            if let dados = snapshot.value as? [String: Any] {
                if let statusR = dados["status"] as? String{
                    self.recarregarTelaStatus(status: statusR, dados: dados)
                    
                }
            }
        })
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //RECUPERA STATUS E AJUSTA INTERFACE
        let datataBase = Database.database().reference()
        
        let requisicoes = datataBase.child("requisicoes")
        let consultaRequisicoes = requisicoes.queryOrdered(byChild: "email")
                                             .queryEqual(toValue: self.emailPassageiro)
        
        consultaRequisicoes.observeSingleEvent(of: DataEventType.childAdded, with:{ (snapshot) in
            if let dados = snapshot.value as? [String: Any] {
                if let statusR = dados["status"] as? String{
                    self.recarregarTelaStatus(status: statusR, dados: dados)
                    
                }
            }
        })
        
    }
    
    func recarregarTelaStatus(status: String, dados: [String: Any])  {
        //CARREGAR TELA BASEADO NOS STATUS
        if status == StatusCorrida.PegarPassageiro.rawValue{
            print("status: PegarPassageiro")
            self.pegarPassageiro()
            
            self.exibeMotoristaPassageiro(lPartida: self.localMotorista, lDestino: self.localPassageiro, tPartida: "Meu local", tDestino: "Passageiro")
        }else if(status == StatusCorrida.IniciarViagem.rawValue){
            print("status: IniciarViagem")
            self.status = .IniciarViagem
            self.alternarBtnIniciarViagem()
            
            //RECUPERA LOCAL DE DESTINO
            if let latDestino = dados["destinoLatitude"] as? Double{
                if let lonDestino = dados["destinoLongitude"] as? Double{
                    //CONFIGURA LOCAL DE DESTINO
                    self.localDestino = CLLocationCoordinate2D(latitude: latDestino, longitude: lonDestino)
                }
            }
            
            //EXIBIR MOTORISTA DESTINO
            self.exibeMotoristaPassageiro(lPartida: self.localMotorista, lDestino: self.localDestino, tPartida: "Motorista", tDestino: "Passageiro")
            
        }else if(status == StatusCorrida.EmViagem.rawValue){
            //ALTERAR O STATUS
            self.status = .EmViagem
            
            //ALTERNA BOTAO
            self.alternarBtnPendenteFinalizarViagem()
            
            //ATUALIZA LOCAL DO MOTORISTA E PASSAGEIRO
            //RECUPERA LOCAL DE DESTINO
            if let latDestino = dados["destinoLatitude"] as? Double{
                if let lonDestino = dados["destinoLongitude"] as? Double{
                    //CONFIGURA LOCAL DE DESTINO
                    self.localDestino = CLLocationCoordinate2D(latitude: latDestino, longitude: lonDestino)
                    
                    //EXIBIR MOTORISTA PASSAGEIRO
                    self.exibeMotoristaPassageiro(lPartida: self.localPassageiro, lDestino: self.localDestino, tPartida: "Motorista", tDestino: "Destino")
                }
            }
        }else if(status == StatusCorrida.ViagemFinalizada.rawValue){
            self.status = .ViagemFinalizada
            if let preco = dados["precoViagem"] as? Double{
                    self.alternarBtnViagemFinalizada(preco: preco)
            }
            
        }
        
    }
    
    @IBAction func aceitarCorrida(_ sender: Any) {
        
        if self.status == StatusCorrida.EmRequisicao{
            //ATUALIZAR REQUISICAO
            let dataBase = Database.database().reference()
            let autenticacao = Auth.auth()
            let requisicoes = dataBase.child("requisicoes")
            
            if let emailMotorista = autenticacao.currentUser?.email {
                requisicoes.queryOrdered(byChild: "email")
                    .queryEqual(toValue: self.emailPassageiro)
                    .observeSingleEvent(of: DataEventType.childAdded, with: {(snapshot) in
                        let dadosMotorista = [
                            "motoristaEmail" : emailMotorista,
                            "motoristaLatitude" : self.localMotorista.latitude,
                            "motoristaLongitude" : self.localMotorista.longitude,
                            "status" : StatusCorrida.PegarPassageiro.rawValue
                            ] as [String : Any]
                        
                        snapshot.ref.updateChildValues(dadosMotorista)
                        self.pegarPassageiro()
                    })
            }
            //EXIBIR CAMINHO PARA O PASSAGEIRO NO MAPA
            let passageiroCLL = CLLocation(latitude: localPassageiro.latitude, longitude: localPassageiro.longitude)
            
            CLGeocoder().reverseGeocodeLocation(passageiroCLL) { (local, erro) in
                if erro == nil{
                    
                    if let dadosLocal = local?.first {
                        let placeMark = MKPlacemark(placemark: dadosLocal)
                        let mapaItem = MKMapItem(placemark: placeMark)
                        mapaItem.name = self.nomePassageiro
                        
                        let opcoes = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
                        mapaItem.openInMaps(launchOptions: opcoes)
                        
                    }
                }
            }
        }else if(self.status == StatusCorrida.IniciarViagem){
            self.iniciarViagemDestino()
        }else if(self.status == StatusCorrida.EmViagem){
            self.finalizarViagem()
        }
    }
    
    func finalizarViagem()  {
        //ALTERA STATUS
        self.status = .ViagemFinalizada
        
        //CALCULA O PRECO DA VIAGEM
        let precoKM: Double = 4
        
        //RECUPERA DADOS PARA ATUALIZAR PRECO
        let dataBase = Database.database().reference()
        let requisicoes = dataBase.child("requisicoes")
        let consultaRequisicoes = requisicoes.queryOrdered(byChild: "email")
                                             .queryEqual(toValue: self.emailPassageiro)
        
        consultaRequisicoes.observeSingleEvent(of: DataEventType.childAdded, with: { (snapshot) in
            if let dados = snapshot.value as? [String: Any]{
                if let latI = dados["latitude"] as? Double{
                    if let lonI = dados["longitude"] as? Double{
                        if let lonD = dados["destinoLongitude"] as? Double{
                            if let latD = dados["destinoLatitude"] as? Double{
                               
                                let inicioLocation = CLLocation(latitude: latI, longitude: lonI)
                                
                                let destinoLocation = CLLocation(latitude: latD, longitude: lonD)
                                
                                //CALCULAR A DISTANCIA
                                let distancia = inicioLocation.distance(from: destinoLocation)
                                let distanciaKM = distancia / 1000
                                let precoViagem = precoKM * distanciaKM
                                
                                let dadosAtualizar = [
                                    "precoViagem": precoViagem,
                                    "distanciaPercorrida": distanciaKM
                                ]
                                snapshot.ref.updateChildValues(dadosAtualizar)
                                
                                //ATUALIZAR REQUISICAO NO FIREBASE
                                self.atualizarStatusRequisicao(status: self.status.rawValue)
                                
                                // ALTERAR PARA VIAGEM FINALIZADA
                                self.alternarBtnViagemFinalizada(preco: precoViagem)
                            }
                        }
                    }
                }
            }
        })
    }
    
 
    
    func iniciarViagemDestino() {
        //ALTERA STATUS
        self.status = .EmViagem
        
        // ATUALIZAR REQUISISCAO NO FIREBASE
        self.atualizarStatusRequisicao(status: self.status.rawValue)
        
        //EXIBIR CAMINHO PARA O DESTINO NO MAPA
        let destinoCLL = CLLocation(latitude: localDestino.latitude, longitude: localDestino.longitude)
        
        CLGeocoder().reverseGeocodeLocation(destinoCLL) { (local, erro) in
            if erro == nil{
                
                if let dadosLocal = local?.first {
                    let placeMark = MKPlacemark(placemark: dadosLocal)
                    let mapaItem = MKMapItem(placemark: placeMark)
                    mapaItem.name = "Destino passageiro"
                    
                    let opcoes = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
                    mapaItem.openInMaps(launchOptions: opcoes)
                    
                }
            }
        }
    }
    
    func pegarPassageiro(){
        //ALTERAR O STATUS
        self.status = .PegarPassageiro
        //ALTERAR BOTAO
        self.alternarBtnPegarPassageiro()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alternarBtnPegarPassageiro() {
        self.btnAceitarCorrida.setTitle("A caminho do passageiro", for: .normal)
        self.btnAceitarCorrida.isEnabled = false
        self.btnAceitarCorrida.backgroundColor = UIColor(displayP3Red: 0.502, green: 0.502, blue: 0.502, alpha: 1)
        
    }
    
    func alternarBtnViagemFinalizada(preco: Double) {
        
        self.btnAceitarCorrida.isEnabled = false
        self.btnAceitarCorrida.backgroundColor = UIColor(displayP3Red: 0.502, green: 0.502, blue: 0.502, alpha: 1)
        
        //FORMATA NUMERO
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.maximumFractionDigits = 2
        nf.locale = Locale(identifier: "pt_BR")
        
        let precoFinal = nf.string(from: NSNumber(value: preco))
        self.btnAceitarCorrida.setTitle("Viagem finalizada - R$" + precoFinal!, for: .normal)
    }
    
    func alternarBtnIniciarViagem() {
        self.btnAceitarCorrida.setTitle("Iniciar Viagem", for: .normal)
        self.btnAceitarCorrida.isEnabled = true
        self.btnAceitarCorrida.backgroundColor = UIColor(displayP3Red: 0.067, green: 0.576, blue: 0.604, alpha: 1)
        
    }
    
    func alternarBtnPendenteFinalizarViagem() {
        self.btnAceitarCorrida.setTitle("Finalizar Viagem", for: .normal)
        self.btnAceitarCorrida.isEnabled = true
        self.btnAceitarCorrida.backgroundColor = UIColor(displayP3Red: 0.067, green: 0.576, blue: 0.604, alpha: 1)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
