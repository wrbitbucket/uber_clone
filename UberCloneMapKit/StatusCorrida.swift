//
//  StatusCorrida.swift
//  UberCloneMapKit
//
//  Created by Wagner Rodrigues on 22/12/2017.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

enum StatusCorrida: String {
    case EmRequisicao, PegarPassageiro, IniciarViagem, EmViagem, ViagemFinalizada
}
